package com.jmachado.searchcompany.data.model

import com.google.gson.annotations.SerializedName

data class Enterprise(
    @field:SerializedName("id") val id: String,
    @field:SerializedName("enterprise_name") val enterprise_name: String,
    @field:SerializedName("description") val description: String,
    @field:SerializedName("city") val city: String,
    @field:SerializedName("country") val country: String,
    @field:SerializedName("photo") val photo: String,
    @field:SerializedName("enterprise_type") val enterpriseType: EnterpriseType)
