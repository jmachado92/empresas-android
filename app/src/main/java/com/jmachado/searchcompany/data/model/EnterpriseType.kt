package com.jmachado.searchcompany.data.model

import com.google.gson.annotations.SerializedName

data class EnterpriseType(
    @field:SerializedName("id") val enterprise_type_id: String,
    @field:SerializedName("enterprise_type_name") val enterprise_type_name : String)
