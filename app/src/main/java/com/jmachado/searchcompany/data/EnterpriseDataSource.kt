package com.jmachado.searchcompany.data

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jmachado.searchcompany.data.model.Enterprise
import com.jmachado.searchcompany.network.RetrofitService

class EnterpriseDataSource (){
    private val initialList = setInitialList()
    private val enterpriseLiveData = MutableLiveData(initialList)

    private val mutableSelectedItem = MutableLiveData<Enterprise>()
    val selectedItem: LiveData<Enterprise> get() = mutableSelectedItem

    fun setInitialList(): List<Enterprise>{
        return listOf()
    }

    fun addEnterprise(enterprise: Enterprise){
        val currentList = enterpriseLiveData.value
        if (currentList == null) {
            enterpriseLiveData.postValue(listOf(enterprise))
        } else {
            val updatedList = currentList.toMutableList()
            updatedList.add(enterprise)
            enterpriseLiveData.postValue(updatedList)
        }
    }

    fun addListEnterprise(currentList: List<Enterprise>){
        enterpriseLiveData.postValue(currentList)
    }

    fun getCompanyList(): LiveData<List<Enterprise>> {
        return enterpriseLiveData
    }

    fun setSelectItem(item: Enterprise) {
        mutableSelectedItem.value = item
    }


    suspend fun getEnterprisesByName(search:String,token:String, client:String,user_uid:String) =
        RetrofitService.getInstance().getEnterprisesByName(token,client,user_uid,search)

    companion object {
        private var INSTANCE: EnterpriseDataSource? = null

        fun getDataSource(): EnterpriseDataSource {
            return synchronized(EnterpriseDataSource::class) {
                val newInstance = INSTANCE ?: EnterpriseDataSource()
                INSTANCE = newInstance
                newInstance
            }
        }
    }
}