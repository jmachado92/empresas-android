package com.jmachado.searchcompany.data

import android.content.Context
import com.jmachado.searchcompany.data.model.LoggedInUser

class SharedPrefManager private constructor(private val mCtx: Context) {

    val isLoggedIn: Boolean
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            val logged = sharedPreferences.getString("token", null)
            return logged != null
        }

    val user: LoggedInUser
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return LoggedInUser(null,
                sharedPreferences.getString("token", null),
                sharedPreferences.getString("client", null),
                sharedPreferences.getString("uid", null)
            )
        }


    fun saveUser(user: LoggedInUser) {

        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

        with(sharedPreferences.edit()){
            putString("token", user.token)
            putString("client", user.client)
            putString("uid", user.uid)
            apply()
        }
    }

    fun clear() {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private val SHARED_PREF_NAME = "my_shared_preff"
        private var mInstance: SharedPrefManager? = null
        @Synchronized
        fun getInstance(mCtx: Context): SharedPrefManager {
            if (mInstance == null) {
                mInstance = SharedPrefManager(mCtx)
            }
            return mInstance as SharedPrefManager
        }
    }

}