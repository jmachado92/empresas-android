package com.jmachado.searchcompany.data

import com.jmachado.searchcompany.data.model.LoggedInUser
import com.jmachado.searchcompany.network.RetrofitService

class LoginRepository() {

    var user: LoggedInUser? = null
        private set

    init {
        user = null
    }

    suspend fun login(username: String, password: String) = RetrofitService.getInstance().signIn(username,password)

}