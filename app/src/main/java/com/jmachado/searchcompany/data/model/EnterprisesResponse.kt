package com.jmachado.searchcompany.data.model

data class EnterprisesResponse (val enterprises: List<Enterprise>)