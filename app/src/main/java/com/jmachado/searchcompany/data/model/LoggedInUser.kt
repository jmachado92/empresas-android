package com.jmachado.searchcompany.data.model

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class LoggedInUser(
        val user: User? = null,
        val token:String? = null,
        val client: String? =null,
        val uid: String? = null
)