package com.jmachado.searchcompany.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jmachado.searchcompany.data.LoginRepository
import com.jmachado.searchcompany.data.model.LoggedInUser
import kotlinx.coroutines.*

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val errorMessage = MutableLiveData<String>()
    val userLogged = MutableLiveData<LoggedInUser>()
    val loading = MutableLiveData<Boolean>()

    fun login(username: String, password: String) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = loginRepository.login(username,password)
            withContext(Dispatchers.Main) {

                if (response.isSuccessful) {
                    val headers = response.headers()
                    if(headers!=null) {
                        userLogged.postValue(
                            LoggedInUser(response.body()!!,headers.get("access-token"),headers.get("client"),headers.get("uid"))
                        )
                        loading.value = false
                    }
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}