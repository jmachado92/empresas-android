package com.jmachado.searchcompany.ui.home

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jmachado.searchcompany.data.EnterpriseDataSource
import com.jmachado.searchcompany.data.model.Enterprise
import com.jmachado.searchcompany.data.model.LoggedInUser
import kotlinx.coroutines.*

class EnterpriseListViewModel(val dataSource: EnterpriseDataSource) : ViewModel() {

    val enterpriseLiveData = dataSource.getCompanyList()
    var selectedItem = dataSource.selectedItem

    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val errorMessage = MutableLiveData<String>()

    fun setSelectItem(item: Enterprise) {
        dataSource.setSelectItem(item)
    }

    fun enterpriseInsert(enterprise: Enterprise){
        dataSource.addEnterprise(enterprise)
    }

    fun enterpriseInsertList(enterpriseList:List<Enterprise>){
        dataSource.addListEnterprise(enterpriseList)
    }

    fun getEnterprises(search:String,token:String, client:String,user_uid:String){

        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = dataSource.getEnterprisesByName(search,token,client,user_uid)
            withContext(Dispatchers.Main) {

                if (response.isSuccessful) {
                    dataSource.addListEnterprise(response.body()?.enterprises!!)
                } else {
                    dataSource.addListEnterprise(dataSource.setInitialList())
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    private fun onError(message: String) {
        errorMessage.value = message
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}

class EnterpriseListViewModelFactory() : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EnterpriseListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return EnterpriseListViewModel(
                dataSource = EnterpriseDataSource.getDataSource()
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}