package com.jmachado.searchcompany.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jmachado.searchcompany.R
import com.jmachado.searchcompany.data.SharedPrefManager
import com.jmachado.searchcompany.databinding.ActivityLoginBinding
import com.jmachado.searchcompany.ui.home.HomeActivity

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val username = binding.username
        val password = binding.password
        val login = binding.login
        val loading = binding.loading
        val txtInvalidLogin = binding.txtInvalidlogin

        loginViewModel = ViewModelProvider(this, LoginViewModelFactory())
                .get(LoginViewModel::class.java)

        username.afterTextChanged {
            login.isEnabled = true
            login.setBackgroundColor(resources.getColor(R.color.button));
            txtInvalidLogin.visibility= View.GONE
        }

        password.apply {
            afterTextChanged {
                login.isEnabled = true
                login.setBackgroundColor(resources.getColor(R.color.button));
                txtInvalidLogin.visibility= View.GONE
            }

            login.setOnClickListener {
                loading.visibility = View.VISIBLE
                loginViewModel.login(username.text.toString(), password.text.toString())
                //loginViewModel.login("testeapple@ioasys.com.br", "12341234")

            }
        }

        loginViewModel.userLogged.observe(this, {
            Toast.makeText(this, it.user?.investor?.investor_name, Toast.LENGTH_SHORT).show()
            SharedPrefManager.getInstance(applicationContext).saveUser(it)

            val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)

        })

        loginViewModel.loading.observe(this, Observer {
            if (it) {
                loading.visibility = View.VISIBLE
            } else {
                loading.visibility = View.GONE
            }
        })

        loginViewModel.errorMessage.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                txtInvalidLogin.visibility = View.GONE
                login.isEnabled = true
                login.setBackgroundColor(resources.getColor(R.color.button));
            } else {
                txtInvalidLogin.visibility = View.VISIBLE
                login.isEnabled = false
                login.setBackgroundColor(resources.getColor(R.color.steel_grey));
            }
        })
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, R.string.invalid_login.toString(), Toast.LENGTH_SHORT).show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}