package com.jmachado.searchcompany.ui.home

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.jmachado.searchcompany.R
import com.jmachado.searchcompany.data.SharedPrefManager
import com.jmachado.searchcompany.data.model.Enterprise
import com.jmachado.searchcompany.databinding.FragmentEnterpriseListBinding
import com.jmachado.searchcompany.ui.adapter.EnterpriseAdapter


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class EnterpriseListFragment : Fragment() {

    private var _binding: FragmentEnterpriseListBinding? = null

    private lateinit var user_token:String
    private lateinit var user_client:String
    private lateinit var user_uid:String

    private var isSearch = false
    private val binding get() = _binding!!

    private lateinit var adapter:EnterpriseAdapter

    private val enterpriseListViewModel by viewModels<EnterpriseListViewModel> {
        EnterpriseListViewModelFactory()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentEnterpriseListBinding.inflate(inflater, container, false)

        adapter = EnterpriseAdapter{ enterprise -> adapterOnClick(enterprise)}
        binding.recyclerView.adapter = adapter

        enterpriseListViewModel.enterpriseLiveData.observe(requireActivity(),{
            it?.let{

                binding.txtListempty.isVisible = it.isEmpty() && isSearch
                if(it.isNotEmpty())
                    adapter.submitList(it as MutableList<Enterprise>)
                else
                    adapter.submitList(listOf<Enterprise>())
            }
        })

        SetLoggedUser()
        setHasOptionsMenu(true);
        return binding.root

    }

    fun SetTextVisible(){
        val list = enterpriseListViewModel.enterpriseLiveData.value
        binding.txtListempty.visibility = View.GONE
        binding.textviewFirst.isVisible = list?.size!! <= 0
    }

    fun SetLoggedUser(){

        if(SharedPrefManager.getInstance(requireContext()).isLoggedIn){
            val user = SharedPrefManager.getInstance(requireContext()).user
            user_token = user.token!!
            user_uid = user.uid!!
            user_client = user.client!!
        }
    }


    private fun adapterOnClick(enterprise: Enterprise) {
        enterpriseListViewModel.setSelectItem(enterprise)
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        SetTextVisible()
        binding.recyclerView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.options_menu, menu)

        val search = menu.findItem(R.id.item_search)
        val searchView = search?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                enterpriseListViewModel.getEnterprises(p0!!,user_token,user_client,user_uid)
                isSearch = true
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                binding.textviewFirst.isVisible = false
                return true
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

}