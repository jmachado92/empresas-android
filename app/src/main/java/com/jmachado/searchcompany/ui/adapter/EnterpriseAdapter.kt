package com.jmachado.searchcompany.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.jmachado.searchcompany.R
import com.jmachado.searchcompany.data.model.Enterprise

class EnterpriseAdapter(private val onClick: (Enterprise) -> Unit) :
    ListAdapter<Enterprise, EnterpriseAdapter.EnterpriseViewHolder>(EnterpriseDiffCallback) {

    class EnterpriseViewHolder (itemView: View, val onClick: (Enterprise) -> Unit) :
        RecyclerView.ViewHolder(itemView) {

        private val enterpriseTextView: TextView = itemView.findViewById(R.id.enterprise_name)
        private val enterpriseImageView: ImageView = itemView.findViewById(R.id.enterprise_image)
        private val enterpriseTypeView: TextView = itemView.findViewById(R.id.enterprise_type)
        private val enterpriseCountryView: TextView = itemView.findViewById(R.id.enterprise_country)
        private var currentEnterprise: Enterprise? = null
        private val cardView: CardView = itemView.findViewById(R.id.card_view)

        init {

            itemView.setOnClickListener {
                currentEnterprise?.let {
                    onClick(it)
                }
            }
        }

        fun bind(enterprise: Enterprise) {
            currentEnterprise = enterprise

            enterpriseTextView.text = enterprise.enterprise_name
            enterpriseTypeView.text = enterprise.enterpriseType.enterprise_type_name
            enterpriseCountryView.text = enterprise.country

            if (enterprise.photo != null) {
                val uri = "https://empresas.ioasys.com.br/" + enterprise.photo
                enterpriseImageView.load(uri)
            } else {
                enterpriseImageView.setImageResource(R.drawable.ic_launcher_background)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterpriseViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return EnterpriseViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: EnterpriseViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

}

object EnterpriseDiffCallback : DiffUtil.ItemCallback<Enterprise>() {
    override fun areItemsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean {
        return oldItem.id == newItem.id
    }
}