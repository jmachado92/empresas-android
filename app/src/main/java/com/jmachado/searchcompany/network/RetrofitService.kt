package com.jmachado.searchcompany.network

import com.jmachado.searchcompany.data.model.EnterprisesResponse
import com.jmachado.searchcompany.data.model.User
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface RetrofitService {
    @FormUrlEncoded
    @POST("users/auth/sign_in")
    suspend fun signIn(
        @Field("email") email:String,
        @Field("password") password: String
    ): Response<User>

    @GET("enterprises")
    suspend fun getEnterprisesByName(
        @Query("access-token") token:String,
        @Query("client") client:String,
        @Query("uid") uid:String,
        @Query("name") name: String
    ): Response<EnterprisesResponse>

    companion object {
        var retrofitService: RetrofitService? = null
        fun getInstance() : RetrofitService {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://empresas.ioasys.com.br/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }

    }
}